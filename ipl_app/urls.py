from django.urls import path
from ipl_app import views

urlpatterns = [
    path('', views.home),
    path('matchesplayedperyear/', views.no_of_matches_played_per_year, name="matchesplayedyear"),
    path('matcheswonoverallyears/', views.matches_won_overall_years, name="matcheswonoverallyears"),
    path('extrarunsconceded/', views.extra_runs_conceded, name="extrarunsconceded"),
    path('topeconomicalbowlers/', views.top_economical_bowlers, name="topeconomicalbowlers"),
    path('highestrunscorers/', views.highest_run_scorers, name="highestrunscorers"),
    path('plotmatchesplayedperyear/', views.plot_no_of_matches_played_per_year, name="plotmatchesplayedyear"),
    path('plotmatcheswonoverallyears/', views.plot_matches_won_overall_years, name="plotmatcheswonoverallyears"),
    path('plotextrarunsconceded/', views.plot_extra_runs_conceded, name="plotextrarunsconceded"),
    path('plottopeconomicalbowlers/', views.plot_top_economical_bowlers, name="plottopeconomicalbowlers"),
    path('plothighestrunscorers/', views.plot_highest_run_scorers, name="plothighestrunscorers"),
]